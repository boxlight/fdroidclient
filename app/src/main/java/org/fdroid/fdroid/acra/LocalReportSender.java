package org.fdroid.fdroid.acra;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.widget.Toast;

import org.acra.ACRA;
import org.acra.ACRAConstants;
import org.acra.ReportField;
import org.acra.collector.CrashReportData;
import org.acra.config.ACRAConfiguration;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;
import org.fdroid.fdroid.FDroidApp;
import org.fdroid.fdroid.R;
import org.fdroid.fdroid.installer.Installer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class LocalReportSender implements ReportSender {

    private static String LOG_PREFIX = "acra_";

    private final Map<ReportField, String> mMapping = new HashMap<ReportField, String>() ;
    private FileOutputStream crashReport = null;
    private final ACRAConfiguration config;

    private String dest;
    public LocalReportSender(ACRAConfiguration config, Context ctx) {
        this.config = config;

        if (hasStoragePermission(ctx)) {
            // the destination
            try {
                File fileDest = new File(ctx.getExternalFilesDir(null), "acra-logs");
                if (!fileDest.exists())
                    fileDest.mkdir();

                dest = fileDest.toString();

                Calendar dateToFormat = new GregorianCalendar();
                String formattedDate = LOG_PREFIX;

                formattedDate += Integer.toString(dateToFormat.get(GregorianCalendar.DAY_OF_MONTH)) + "-";
                formattedDate += Integer.toString(dateToFormat.get(GregorianCalendar.MONTH) + 1) + "-";
                formattedDate += Integer.toString(dateToFormat.get(GregorianCalendar.YEAR)) + ".log";

                File d = new File(dest);
                if (!d.exists()) Log.d("Make dir", "" + d.mkdirs());
                d = new File(dest + "/" + formattedDate);
                if (!d.exists()) Log.d("Make file", "" + d.createNewFile());

                crashReport = new FileOutputStream(dest + "/" + formattedDate, true);
            } catch (FileNotFoundException e) {
                Log.e("TAG", "IO ERROR", e);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(ctx,"No storage permission to write logs",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void send(Context context, CrashReportData report) throws ReportSenderException {  //dato il crash , ossia i lsecondo argomento

        if (hasStoragePermission(context)) {

            final Map<String, String> finalReport = remap(report);

            try {
                OutputStreamWriter osw = new OutputStreamWriter(crashReport);

                Set set;
                set = finalReport.entrySet();
                Iterator i = set.iterator();

                while (i.hasNext()) {
                    Map.Entry<String, String> me = (Map.Entry) i.next();
                    osw.write("[" + me.getKey() + "] [ " + String.valueOf(new Timestamp((new Date()).getTime())) + " ]=" + me.getValue());
                }

                osw.flush();
                osw.close();
            } catch (IOException e) {

            }
        }
        else
        {
            Toast.makeText(context,"No storage permission to write logs",Toast.LENGTH_LONG).show();

            final Map<String, String> finalReport = remap(report);

            try {
                StringBuffer osw = new StringBuffer();

                Set set;
                set = finalReport.entrySet();
                Iterator i = set.iterator();

                while (i.hasNext()) {
                    Map.Entry<String, String> me = (Map.Entry) i.next();
                    osw.append("[" + me.getKey() + "] [ " + String.valueOf(new Timestamp((new Date()).getTime())) + " ]=" + me.getValue()).append("\n");
                }

                Toast.makeText(context,"No storage permission to write logs",Toast.LENGTH_LONG).show();


            } catch (Exception e) {

            }
        }

    }

    private Map<String, String> remap(Map<ReportField, String> report) {

        Set<ReportField> fields = ACRA.getConfig().getReportFields();

        if (fields.size() == 0) {
            for (ReportField field : ACRAConstants.DEFAULT_REPORT_FIELDS)
                fields.add(field);
        }

        final Map<String, String> finalReport = new HashMap<String, String>(
                report.size());
        for (ReportField field : fields) {
            if (mMapping == null || mMapping.get(field) == null) {
                finalReport.put(field.toString(), report.get(field));
            } else {
                finalReport.put(mMapping.get(field), report.get(field));
            }
        }
        return finalReport;
    }

    private boolean hasStoragePermission(Context context) {
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }


}